name "frms"

license "GPL3"

dependency "python-uwsgi"
dependency "python-psycopg2"


env = with_standard_compiler_flags(with_embedded_path)
env['PATH'] = "#{install_dir}/embedded/bin:#{env["PATH"]}"
env['CXXFLAGS'] = "-I#{install_dir}/embedded/include"
env['CPPFLAGS'] = "-I#{install_dir}/embedded/include"
env.delete('CFLAGS')


postgres_dir="#{install_dir}/embedded/postgresql/9.6.11"

build do
    command "git clone https://gitlab.hrz.tu-chemnitz.de/saxid-federated-resource-management/frms.git /opt/frms/software"
    buildopts = "--global-option=\"-R#{postgres_dir}/lib/\" --global-option=\"-L#{postgres_dir}/lib/\" --global-option=\"-I#{postgres_dir}/include/\""
    command "#{install_dir}/embedded/bin/pip install -I --global-option=build_ext #{buildopts} --install-option=\"--install-scripts=#{install_dir}/bin\" -r /opt/frms/software/requirements.txt", env: env
    command "#{install_dir}/embedded/bin/pip install -I --global-option=build_ext #{buildopts} --install-option=\"--install-scripts=#{install_dir}/bin\" 'ipython<6'", env: env

    mkdir "#{install_dir}/etc"

    erb source: "manage.py.erb",
        dest: "#{install_dir}/software/manage.py",
        vars: {install_dir: install_dir},
        mode: 0755

    erb source: "customer_settings.py.erb",
        dest: "#{install_dir}/etc/customer_settings.py.example",
        vars: {install_dir: install_dir}

    erb source: "README.erb",
        dest: "#{install_dir}/README",
        vars: {install_dir: install_dir}

    erb source: "settings.py.erb",
        dest: "#{install_dir}/software/frms/settings/__init__.py"

    mkdir "#{build_dir}/etc/systemd/system"
    erb source: "settings.py.erb",
        dest: "#{build_dir}/etc/systemd/system/foo"
end
project.extra_package_file('/etc/systemd/system/foo')
