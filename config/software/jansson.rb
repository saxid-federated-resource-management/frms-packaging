name "jansson"

default_version "2.10"

version('2.10') { source md5: '16a2c4e84c0a80ca61bd6e619a0f9358' }

source url: "http://www.digip.org/jansson/releases/jansson-#{version}.tar.gz"

relative_path "jansson-#{version}"

build do
    env = with_standard_compiler_flags(with_embedded_path)
    command "./configure --prefix=#{install_dir}/embedded"
    make "-j #{workers}",env: env
    make "-j #{workers} install", env: env
end
