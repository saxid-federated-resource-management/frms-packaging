name "python-uwsgi"

dependency "python-pip"
dependency "libuuid"
dependency "liblzma"
dependency "pcre"
dependency "libattr"
dependency "libcap"
dependency "libxml2"
dependency "jansson"

build do
    env = with_standard_compiler_flags(with_embedded_path)
    command "#{install_dir}/embedded/bin/pip install --no-cache-dir uwsgi", env: env
end
