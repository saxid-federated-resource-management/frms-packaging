name "python-setuptools"

default_version "36.2.7"

dependency "python"

version("36.2.7") { source md5: "b824746259165ba4cf0dccb3b3d6c50e" }
source url: "https://github.com/pypa/setuptools/archive/v#{version}.tar.gz"
relative_path "setuptools-#{version}"



build do
    command "#{install_dir}/embedded/bin/python ./bootstrap.py"
    command "#{install_dir}/embedded/bin/python ./setup.py build"
    command "#{install_dir}/embedded/bin/python ./setup.py install"
end
