name "python"
default_version "2.7.13"

#dependency "gdbm"
dependency "ncurses"
dependency "zlib"
dependency "openssl"
dependency "bzip2"
dependency "sqlite"
version("2.7.5") { source md5: "b4f01a1d0ba0b46b05c73b2ac909b1df" }
version("2.7.9") { source md5: "5eebcaa0030dc4061156d3429657fb83" }
version("2.7.13") { source md5: "17add4bf0ad0ec2f08e0cae6d205c700" }
source url: "http://python.org/ftp/python/#{version}/Python-#{version}.tgz"
relative_path "Python-#{version}"


build do
    env = {
      "CFLAGS" => "-I#{install_dir}/embedded/include -O3 -g -pipe",
      "LDFLAGS" => "-Wl,-rpath,#{install_dir}/embedded/lib -L#{install_dir}/embedded/lib",
      "MACOSX_DEPLOYMENT_TARGET" => "10.11",
      "PYTHON_DISABLE_MODULES" => "_tkinter",
    }
    command "./configure" \
          " --prefix=#{install_dir}/embedded" \
          " --enable-shared" \
          " --without-tkinter" \
          " --without-freetype" \
          " --with-dbmliborder=", env: env

    make env: env
    make "install", env: env

    # There exists no configure flag to tell Python to not compile readline
    delete "#{install_dir}/embedded/lib/python2.7/lib-dynload/readline.*"

    # Remove unused extension which is known to make healthchecks fail on CentOS 6
    delete "#{install_dir}/embedded/lib/python2.7/lib-dynload/_bsddb.*"
    # Ditto for sqlite3
    delete "#{install_dir}/embedded/lib/python2.7/lib-dynload/_sqlite3.*"
    delete "#{install_dir}/embedded/lib/python2.7/sqlite3/"

    # Remove unused extension which is known to make healthchecks fail on CentOS 7
    delete "#{install_dir}/embedded/lib/python2.7/lib-dynload/dbm.so"

    # Remove sqlite3 libraries, if you want to include sqlite, create a new def
    # in your software project and build it explicitly. This removes the adapter
    # library from python, which links incorrectly to a system library. Adding
    # your own sqlite definition will fix this.
    delete "#{install_dir}/embedded/lib/python2.7/lib-dynload/_tkinter.*"
end

