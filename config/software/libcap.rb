name "libcap"

default_version '2.25'


version('2.25') { source md5: '4b18f7166a121138cca0cdd8ab64df4c' }

basever = "#{version.to_s.split(".")[0..1].join(".")}"
source url: "https://www.kernel.org/pub/linux/libs/security/linux-privs/libcap2/libcap-#{version}.tar.gz"
relative_path "libcap-#{version}"

build do
    env = with_standard_compiler_flags(with_embedded_path)
    #command "./configure --prefix=#{install_dir}/embedded --disable-all-programs --enable-libuuid --disable-bash-completion"
    make "-j #{workers}",env: env
    make "-C libcap/ FAKEROOT=#{install_dir}/embedded install", env: env
    ["libcap.so", "libcap.a", "libcap.so.#{version}", "libcap.so.2"].each{|f|
        move "#{install_dir}/embedded/lib64/#{f}", "#{install_dir}/embedded/lib/#{f}"
    }
end
