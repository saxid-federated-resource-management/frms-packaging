name "python-psycopg2"

default_version "2.7.6"

dependency "python"
dependency "python-pip"
dependency "postgresql"

#version("2.7.3") { source md5: "f9823ffedcec57a8c036e67c6fb3fa36" }
#source url: "http://initd.org/psycopg/tarballs/PSYCOPG-2-7/psycopg2-#{version}.tar.gz"
#relative_path "psycopg2-#{version}"

env = with_standard_compiler_flags(with_embedded_path)
env['PATH'] = "#{install_dir}/embedded/bin:#{env["PATH"]}"
env['CXXFLAGS'] = "-L#{install_dir}/embedded/lib -I#{install_dir}/embedded/include"
env['CPPFLAGS'] = "-L#{install_dir}/embedded/lib -I#{install_dir}/embedded/include"

postgres_dir="#{install_dir}/embedded/postgresql/9.6.11"

build do
    buildopts = "--global-option=\"-R#{postgres_dir}/lib/\" --global-option=\"-L#{postgres_dir}/lib/\" --global-option=\"-I#{postgres_dir}/include/\""
    command "#{install_dir}/embedded/bin/pip install -I --global-option=build_ext #{buildopts} --install-option=\"--install-scripts=#{install_dir}/bin\" psycopg2==#{version}", env: env
end

