name "python-pip"

default_version "9.0.1"

dependency "python"
dependency "python-setuptools"

version("9.0.1") { source md5: "ae605ab1ccdcb7f9e219a400b4bc68b6" }
source url: "https://github.com/pypa/pip/archive/#{version}.tar.gz"
relative_path "pip-#{version}"



build do
    command "/usr/bin/pwd > /tmp/buiddir"
    command "#{install_dir}/embedded/bin/python ./setup.py build"
    command "#{install_dir}/embedded/bin/python ./setup.py install"
end
