name "libuuid"

default_version '2.30.1'


version('2.30.1') { source md5: '22a860a2d98367e73e414a09dfaf3866' }

basever = "#{version.to_s.split(".")[0..1].join(".")}"
source url: "https://www.kernel.org/pub/linux/utils/util-linux/v#{basever}/util-linux-#{version}.tar.gz"
relative_path "util-linux-#{version}"

build do
    env = with_standard_compiler_flags(with_embedded_path)
    command "./configure --prefix=#{install_dir}/embedded --disable-all-programs --enable-libuuid --disable-bash-completion"
    make "-j #{workers}",env: env
    make "-j #{workers} install", env: env
end
