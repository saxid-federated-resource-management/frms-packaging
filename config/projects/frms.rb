#
# Copyright 2017 YOUR NAME
#
# All Rights Reserved.
#

name "frms"
maintainer "Daniel Schreiber"
homepage "https://www.tu-chemnitz.de/urz/"

# Defaults to C:/frms on Windows
# and /opt/frms on all other platforms
install_dir "#{default_root}/#{name}"

build_version Omnibus::BuildVersion.semver
build_iteration 1

# Creates required build directories
#dependency "preparation"

# frms dependencies/components
# dependency "somedep"
dependency "python"
dependency "python-pip"
dependency "postgresql"
dependency "mysql-client"
dependency "python-psycopg2"
dependency "python-uwsgi"
dependency "frms"

# Version manifest file
dependency "version-manifest"

exclude "**/.git"
exclude "**/bundler/git"
